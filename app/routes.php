<?php

$app->group('', function() use ($app, $container) {

	$app->get('/productos', 'HomeController:productos');

	$app->get('/categorias', 'HomeController:categorias');

	$app->get('/marcas', 'HomeController:marcas');
});
