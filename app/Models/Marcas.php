<?php

namespace App\Models;

use \Illuminate\Database\Eloquent\Model;


class Marcas extends Model
{

protected $table = 'Marca';

protected $primaryKey = 'id';

public $timestamps = false;

}
