<?php

namespace App\Models;

use \Illuminate\Database\Eloquent\Model;


class Categorias extends Model
{

protected $table = 'Categoria';

protected $primaryKey = 'id';

public $timestamps = false;

}
