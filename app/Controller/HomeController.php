<?php
namespace App\Controller;

use Httpful\Request;
use App\Models\Categorias;
use App\Models\Marcas;
use App\Models\Productos;

class HomeController
{

	public function productos($request, $response, $args)
	{

		$productos = Productos::select('productos.*', 'Marca.Nombre as NombreMarca', 'Categoria.Nombre as NombreCategoria')
		->join('Marca', 'Marca.id','=', 'productos.idMarca')
		->join('Categoria', 'Categoria.id','=','productos.idCategoria')
		->get();

		return json_encode($productos);

	}
	public function categorias($request, $response, $args)
	{

		$categorias = Categorias::get();

		return json_encode($categorias);

	}
	public function marcas($request, $response, $args)
	{

		$marcas = Marcas::get();

		return json_encode($marcas);

	}

}
